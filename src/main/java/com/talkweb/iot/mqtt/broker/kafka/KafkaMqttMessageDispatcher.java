/*
 * Copyright (c) 2021-2021, talkweb 拓维信息 www.talkweb.com.cn.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.talkweb.iot.mqtt.broker.kafka;

import com.talkweb.iot.mqtt.broker.service.IMqttClusterService;
import net.dreamlu.iot.mqtt.core.server.dispatcher.IMqttMessageDispatcher;
import net.dreamlu.iot.mqtt.core.server.enums.MessageType;
import net.dreamlu.iot.mqtt.core.server.model.Message;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

/**
 * redis 消息转发器
 *
 * @author L.cm
 */
@Configuration(proxyBeanMethods = false)
public class KafkaMqttMessageDispatcher implements IMqttMessageDispatcher, SmartInitializingSingleton {
    @Autowired
    private ApplicationContext context;
    private IMqttClusterService clusterService;

    @Override
    public boolean send(Message message) {
        MessageType messageType = message.getMessageType();
        // 上行消息先发送到上行通道
        if (MessageType.UP_STREAM == messageType) {
            clusterService.sendToKafka(KafkaTopics.TOPIC_MESSAGE_UP, message);
        }
        // 发布到集群交换通道
        clusterService.sendToKafka(KafkaTopics.TOPIC_MESSAGE_EXCHANGE, message);
        return true;
    }

    @Override
    public boolean send(String clientId, Message message) {
        message.setClientId(clientId);
        return send(message);
    }

    @Override
    public void afterSingletonsInstantiated() {
        this.clusterService = context.getBean(IMqttClusterService.class);
    }
}
